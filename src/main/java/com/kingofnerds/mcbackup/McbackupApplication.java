package com.kingofnerds.mcbackup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class McbackupApplication {

	public static void main(String[] args) {
		SpringApplication.run(McbackupApplication.class, args);
	}

}
